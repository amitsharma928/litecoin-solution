FROM ubuntu:20.04

##########################
# LITECOIN Container
##########################

ENV LITECOIN_VERSION=0.18.1

RUN useradd -mr litecoin \
    && apt-get update \
    && apt-get -y install gnupg curl \
    && curl -so litecoin-linux-gnu.tar.gz https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-x86_64-linux-gnu.tar.gz \
    && curl -so litecoin-linux-signatures.asc https://download.litecoin.org/litecoin-${LITECOIN_VERSION}/linux/litecoin-${LITECOIN_VERSION}-linux-signatures.asc \
    && gpg --keyserver keyserver.ubuntu.com --recv-keys FE3348877809386C \
    && gpg --verify litecoin-linux-signatures.asc \
    # Verify checksum
    && grep "$(sha256sum litecoin-linux-gnu.tar.gz | awk '{print $1}')" litecoin-linux-signatures.asc \
    && tar xf litecoin-linux-gnu.tar.gz \
    && install -m 0755 -o litecoin -g litecoin -t /usr/local/bin litecoin-$LITECOIN_VERSION/bin/* \
    && rm -r litecoin-$LITECOIN_VERSION \ 
    && rm litecoin-linux-gnu.tar.gz \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /var/tmp/* /tmp/*

USER litecoin

VOLUME ["/opt/litecoin"]

EXPOSE 9333

ENTRYPOINT ["litecoind"]
