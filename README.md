# Litecoin build and deployment

## Prerequisite

- Docker
- K8s cluster - For ex. - minikube
- Python3+
- Git

## Task 1

Write a [Dockerfile](./Dockerfile) file to build a docker image to run litecoin version 0.18.1 container.

```shell
$ docker build -t litecoin:0.18.1 .
$ docker run litecoin:0.18.1
2022-03-13T01:33:36Z Litecoin Core version v0.18.1 (release build)
...

# Docker container image security scan using Anchore
curl -s https://ci-tools.anchore.io/inline_scan-v0.6.0 | bash -s -- -f -d Dockerfile litecoin:0.18.1

# Result of security scan:
Image Digest: sha256:260630331cb720fd25a222bcb024ce22aa1f0e333430d59d285a54c095f0a134
Full Tag: localhost:5000/litecoin:0.18.1
Status: pass
Last Eval: 2022-03-13T02:03:48Z
Policy ID: 2c53a13c-1765-11e8-82ef-23527761d060
```

### Reference used-

- <https://download.litecoin.org/README-HOWTO-GPG-VERIFY-TEAM-MEMBERS-KEY.txt>
- <https://docs.docker.com/engine/reference/builder/>
- <https://anchore.com/blog/inline-scanning-with-anchore-engine/>

## Task 2

Write a Kubernetes [StatefulSet](./statefulset.yaml) to run above docker container using persistent volume claims and resource limits.

```shell
# First push docker image to dockerhub which is later used by statefulset to start the pod. Minikube local registry can also be used.
$ docker tag litecoin:0.18.1 amitsharma928/litecoin:0.18.1
$ docker push amitsharma928/litecoin:0.18.1

# Validate file using kubeval https://kubeval.instrumenta.dev/
$ kubeval statefulset.yaml         
PASS - statefulset.yaml contains a valid StatefulSet (litecoin

$ kubectl apply -f statefulset.yaml
statefulset.apps/litecoin created

$ kubectl get statefulsets.apps litecoin
NAME       READY   AGE
litecoin   1/1     25s

$ kubectl get pvc
NAME                     STATUS   VOLUME                                     CAPACITY   ACCESS MODES   STORAGECLASS   AGE
litecoin-pv-litecoin-0   Bound    pvc-a76cb79e-c6ff-4d92-93e0-dd8b7679fd0f   1536Mi     RWO            standard       45s

$ kubectl get pods
NAME         READY   STATUS    RESTARTS   AGE
litecoin-0   1/1     Running   0          103s

```

### Reference used -

- <https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/>
- <https://kubernetes.io/docs/concepts/storage/persistent-volumes/>

## Task 3

Write build and deployment pipeline using [GitLab-CI](./.gitlab-ci.yml).

There are 2 stages in .gitlab-ci.yml.

- build - It builds docker image and pushes to <https://hub.docker.com/>
- deploy-k8s - It deploys statefulset to k8s.

Pipeline URL - <https://gitlab.com/amitsharma928/litecoin-solution/-/pipelines/490675329>

**Note:** deploy-k8s stage is failing because no remote cluster is configured. It will work with a running remote cluster.

### Reference used -

- <https://pythonspeed.com/articles/gitlab-build-docker-image/>

- <https://aliartiza75.medium.com/gitlab-integration-with-kubernetes-46c2473a4396>

## Task 4

**Que** - A csv file contains 3 comma separate fields. Print 1st and 3rd field only in console, these should be converted to upper case and should be separated by | (vertical bar/pipe).

[task4_data.csv](./task4_data.csv) file is used for sample.

```shell
$ cat task4_data.csv
name,age,marks
mukesh,19,90
ram,20,85
david,18,83

$ awk -F',' '{print $1,"|",$3}' task4_data.csv |tr "[:lower:]" "[:upper:]"          
NAME | MARKS
MUKESH | 90
RAM | 85
DAVID | 83

```

## Task 5

Implemention of **Task 4** in Python. Script name is [task5_csv.py](./task5_csv.py)

```shell
$ cat task4_data.csv 
name,age,marks
mukesh,19,90
ram,20,85
david,18,83

$ python task5_csv.py task4_data.csv 
NAME | MARKS
MUKESH | 90
RAM | 85
DAVID | 83

# Throw error if no file provided in arg
$ python task5_csv.py               
Provide a csv file in argument
```

### Reference

- <https://www.geeksforgeeks.org/isupper-islower-lower-upper-python-applications/>

## Task 6

I haven't worked with Terraform earlier and working with Chef for IaaC and config management.
