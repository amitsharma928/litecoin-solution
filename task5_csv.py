#!/usr/bin/env python3

'''
Python program to parse a CSV and print 1st and 3rd field in upper case and separated by | (pipe).
'''

import sys
from csv import reader

# Check if argument is provided
if len(sys.argv) < 2:
    print("Provide a csv file in argument")
    sys.exit(1)

# open file in read mode
with open(sys.argv[1], 'r', encoding="utf8") as read_obj:
    # pass the file object to reader() to get the reader object
    csv_reader = reader(read_obj)
    # Iterate over each row in the csv using reader object
    for row in csv_reader:
        # Print 1st and 3rd field and convert in upper case
        print(row[0].upper(),"|", row[2].upper())
